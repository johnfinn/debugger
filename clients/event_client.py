#!/bin/python2
import zmq
import time
import sys
#from pyparsing import word

context  = zmq.Context()
events   = context.socket(zmq.SUB)
events.connect("ipc:///tmp/events")
events.setsockopt(zmq.SUBSCRIBE, "")


# add new gdb commands to control the server
def client_loop():
    while True:
        message = events.recv()
        print "%s" % message

try:
    client_loop()
except KeyboardInterrupt:
    print "Exiting"
finally:
    events.close()


