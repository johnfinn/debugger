#!/usr/bin/env python

# example-start buttons buttons.py

import pygtk
pygtk.require('2.0')
import gtk

import zmq
context  = zmq.Context()
commands = context.socket(zmq.REQ)
commands.connect("ipc:///tmp/commands")

def xpm_label_box(parent, label_text):
    # Create box for xpm and label
    box1 = gtk.HBox(False, 0)
    box1.set_border_width(0)

    # Create a label for the button
    label = gtk.Label(label_text)

    # Pack the pixmap and label into the box
    box1.pack_start(label, False, False, 3)

    label.show()
    return box1

class Buttons:
    # Our usual callback method
    def callback(self, widget, data=None):
        print "%s was pressed. Sending to gdb" % data
        commands.send("interpreter-exec mi2 \"%s\"" % data)
        message = commands.recv()
        print "received"

    def __init__(self):
        # Create a new window
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)

        self.window.set_title("show version")

        # It's a good idea to do this for all windows.
        self.window.connect("destroy", lambda wid: gtk.main_quit())
        self.window.connect("delete_event", lambda a1,a2:gtk.main_quit())

        # Sets the border width of the window.
        self.window.set_border_width(0)

        self.add_gdb_button("info program")

        self.window.show()

    def add_gdb_button(self, data):
        # Create a new button
        button = gtk.Button()

        # Connect the "clicked" signal of the button to our callback
        button.connect("clicked", self.callback, data)

        # This calls our box creating function
        box = xpm_label_box(self.window, data)

        # Pack and show all our widgets
        button.add(box)

        box.show()
        button.show()

        self.window.add(button)

def main():
    gtk.main()
    return 0     

if __name__ == "__main__":
    Buttons()
    main()

