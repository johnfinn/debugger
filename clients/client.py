#!/bin/python2
import zmq
import time
import sys

context  = zmq.Context()
commands = context.socket(zmq.REQ)
#events   = context.socket(zmq.SUB)
commands.connect("ipc:///tmp/commands")
#events.connect("tcp://*:%s" % 5558)


# add new gdb commands to control the server
def client_loop():
    request = ""
    while True:

        request = raw_input("(gdb) ")
        commands.send("interpreter-exec mi2 \"%s\"" % request)
        #commands.send(request)
        message = commands.recv()
        print "received"
        print "%s" % (message.lstrip("mi2_response: {'").rstrip("'}"))
        request = ""

        #except ZMQError as e:
        #    print "ZMQ error has occurred"
        #    print e

try:
    client_loop()
except KeyboardInterrupt:
    print "Exiting"
finally:
    commands.close()

