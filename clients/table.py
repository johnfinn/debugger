#!/bin/python2
import curses

stdscr = curses.initscr()
curses.noecho()
curses.cbreak()
stdscr.keypad(True)

table = { 
    "test1" : "1",
    "test2" : "2",
    "test3" : "3",
}


#def client_loop():
#    request = ""
#    while True:
#        key = stdscr.getch()
#        if key == curses.KEY_UP:
#            print "KEY UP"
#        elif key == curses.KEY_DOWN:
#            print "KEY DOWN"
#        elif key == curses.KEY_ENTER:
#            commands.send(request)
#            message = commands.recv()
#            print "%s" % (message.lstrip("mi2_response: {'").rstrip("'}"))
#            print("(gdb) ")
#            request = ""
#        else:
#            print curses.keyname(key),
#            request += curses.keyname(key)
#        stdscr.addstr(5,5,request)
#        stdscr.addstr(0,0,curses.keyname(key))
#        stdscr.refresh()

#print table

def print_table(x, y):
    for key in table.keys():
        stdscr.addstr(y,x,key)
        stdscr.refresh()
        y += 1


try:
    print_table(3, 5)
    input = raw_input()
except KeyboardInterrupt:
    print "Exiting"
finally:
    curses.nocbreak()
    stdscr.keypad(False)
    curses.echo()
    curses.endwin()
