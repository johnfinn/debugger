#!/bin/python2
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

from scapy.all import *
import zmq


context  = zmq.Context()
commands = context.socket(zmq.REQ)
commands.connect("ipc:///tmp/commands")

dash_buttons = {
        'a0:02:dc:69:d8:c8' : "show version"
}

def arp_display(pkt):
    if pkt.haslayer(ARP):
        if pkt[ARP].op == 1: #who-has (request)
            if pkt[ARP].psrc == '0.0.0.0': # ARP Probe
                for mac, command in dash_buttons.iteritems():
                    if pkt[ARP].hwsrc == mac:
                        print "command: ", command
                        commands.send("interpreter-exec mi2 \"%s\"" % command)
                        message = commands.recv()

try:
    print sniff(prn=arp_display, filter="arp", store=0, count=0)
except socket.error as e:
    print e
