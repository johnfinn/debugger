#!/bin/python2
import mi2_parser


def echo_test()
"echo hello world"

def version_test()
"show version"

def logging()
"show logging"

def environment()
"show environment"

def miasyn
"show mi-async"

"show annotate"
"show architecture"
"show args"

"info program" # shows the status of the program
"info scope"
"info source"
"info sources"


"info os"
"
cpus       Listing of all cpus/cores on the system
files      Listing of all file descriptors 
modules    Listing of all loaded kernel modules 
msg        Listing of all message queues 
processes  Listing of all processes 
procgroups Listing of all process groups 
semaphores Listing of all semaphores 
shm        Listing of all shared-memory regions 
sockets    Listing of all internet-domain sockets 
threads    Listing of all threads 
"

"info types"

"file"

"run"




tests = {
{
"show version",
r"""
~"GNU gdb (GDB) 7.10\n"
~"Copyright (C) 2015 Free Software Foundation, Inc.\n"
~"License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.  Type \"show copying\"\nand \"show warranty\" for details.\n"
~"This GDB was configured as \"x86_64-unknown-linux-gnu\".\nType \"show configuration\" for configuration details."
~"\nFor bug reporting instructions, please see:\n"
~"<http://www.gnu.org/software/gdb/bugs/>.\n"
~"Find the GDB manual and other documentation resources online at:\n<http://www.gnu.org/software/gdb/documentation/>.\n"
~"For help, type \"help\".\n"
~"Type \"apropos word\" to search for commands related to \"word\".\n"
&"  File \"server.py\", line 51\n"
&"    print \"Event: \", event_to_json(event)\n"
&"                  ^\n"
&"SyntaxError: invalid syntax\n"
~"Startup time: 0.063332 (cpu), 0.059606 (wall)\n"
~"Space used: 8298496 (+8298496 during startup)\n"
(gdb) 
"""
},
{
"10-gdb-version",
r"""
~"GNU gdb (GDB) 7.10\n"
~"Copyright (C) 2015 Free Software Foundation, Inc.\n"
~"License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.  Type \"show copying\"\nand \"show warranty\" for details.\n"
~"This GDB was configured as \"x86_64-unknown-linux-gnu\".\nType \"show configuration\" for configuration details."
~"\nFor bug reporting instructions, please see:\n"
~"<http://www.gnu.org/software/gdb/bugs/>.\n"
~"Find the GDB manual and other documentation resources online at:\n<http://www.gnu.org/software/gdb/documentation/>.\n"
~"For help, type \"help\".\n"
~"Type \"apropos word\" to search for commands related to \"word\".\n"
10^done
"""
},
{
"",
r"""
10^done
"""
},
{
"echo testing",
r"""
&"echo testing\n"
~"testing"
^done
"""
},
{
"info registers",
r"""
info registers
&"info registers\n"
&"The program has no registers now.\n"
^error,msg="The program has no registers now."
"""
},
{
"10-gdb-show",
r"""
10^done,showlist={optionlist={prefix="ada ",showlist={option={name="trust-PAD-over-XVS",value="on"}}},option={name="agent",value="off"},option={name="annotate",value="0"},option={name="architecture"},option={name="args"},option={name="auto-connect-native-target",value="on"},optionlist={prefix="auto-load ",showlist={option={name="gdb-scripts",value="on"},option={name="guile-scripts",value="on"},option={name="libthread-db",value="on"},option={name="local-gdbinit",value="on"},option={name="python-scripts",value="on"},option={name="safe-path",value="$debugdir:$datadir/auto-load"},option={name="scripts-directory",value="$debugdir:$datadir/auto-load"}}},option={name="auto-load-scripts",value="on"},option={name="auto-solib-add",value="on"},optionlist={prefix="backtrace ",showlist={option={name="limit",value="unlimited"},option={name="past-entry",value="off"},option={name="past-main",value="off"}}},option={name="basenames-may-differ",value="off"},optionlist={prefix="breakpoint ",showlist={option={name="always-inserted",value="off"},option={name="auto-hw",value="on"},option={name="condition-evaluation",value="auto"},option={name="pending",value="auto"}}},option={name="c",showlist={option={name="range",value="auto"},option={name="type",value="on"}}},option={name="can-use-hw-watchpoints",value="1"},option={name="case-sensitive",value="auto"},option={name="ch",showlist={option={name="range",value="auto"},option={name="type",value="on"}}},option={name="charset",value="auto"},optionlist={prefix="check ",showlist={option={name="range",value="auto"},option={name="type",value="on"}}},option={name="circular-trace-buffer",value="off"},option={name="code-cache",value="on"},option={name="coerce-float-to-double",value="on"},option={name="compile-args",value="-O0 -gdwarf-4 -fPIE -Wall  -Wno-implicit-function-declaration -Wno-unused-but-set-variable -Wno-unused-variable -fno-stack-protector"},option={name="complaints",value="0"},option={name="confirm",value="on"},option={name="conv"},option={name="convenience"},option={name="cp-abi",cp-abi="auto",longname="currently \"gnu-v3\""},option={name="data-directory"},optionlist={prefix="dcache ",showlist={option={name="line-size",value="64"},option={name="size",value="4096"}}},optionlist={prefix="debug ",showlist={option={name="arch",value="0"},option={name="auto-load",value="off"},option={name="check-physname",value="off"},option={name="coff-pe-read",value="0"},option={name="compile",value="off"},option={name="displaced",value="off"},option={name="dwarf-die",value="0"},option={name="dwarf-line",value="0"},option={name="dwarf-read",value="0"},option={name="entry-values",value="0"},option={name="expression",value="0"},option={name="frame",value="0"},option={name="infrun",value="0"},option={name="jit",value="0"},option={name="libthread-db",value="0"},option={name="lin-lwp",value="0"},option={name="linux-namespaces",value="off"},option={name="notification",value="off"},option={name="observer",value="0"},option={name="overload",value="0"},option={name="parser",value="off"},option={name="py-unwind",value="0"},option={name="record",value="0"},option={name="remote",value="0"},option={name="serial",value="0"},option={name="stap-expression",value="0"},option={name="symbol-lookup",value="0"},option={name="symfile",value="off"},option={name="symtab-create",value="0"},option={name="target",value="0"},option={name="timestamp",value="off"},option={name="varobj",value="0"},option={name="xml",value="off"}}},option={name="debug-file-directory",value="/usr/lib/debug"},option={name="default-collect"},option={name="demangle-style",value="auto"},option={name="detach-on-fork",value="on"},option={name="directories",value="$cdir:$cwd"},option={name="disable-randomization",value="on"},option={name="disassemble-next-line",value="off"},option={name="disassembly-flavor",value="att"},option={name="disconnected-dprintf",value="on"},option={name="disconnected-tracing",value="off"},option={name="displaced-stepping",value="auto"},option={name="dprintf-channel"},option={name="dprintf-function",value="printf"},option={name="dprintf-style",value="gdb"},option={name="editing",value="off"},option={name="endian"},option={name="environment"},option={name="exec-direction",value="forward"},option={name="exec-done-display",value="off"},option={name="exec-wrapper"},option={name="extended-prompt"},option={name="extension-language"},option={name="filename-display",value="relative"},option={name="follow-exec-mode",value="same"},option={name="follow-fork-mode",value="parent"},optionlist={prefix="frame-filter ",showlist={option={name="priority"}}},option={name="gnutarget",value="auto"},option={name="gu",showlist={option={name="print-stack",value="message"}}},optionlist={prefix="guile ",showlist={option={name="print-stack",value="message"}}},option={name="height",value="69"},optionlist={prefix="history ",showlist={option={name="expansion",value="off"},option={name="filename",value="/home/johnfinn/workspace/projects/debugger/clients/.gdb_history"},option={name="remove-duplicates",value="0"},option={name="save",value="off"},option={name="size",value="256"}}},option={name="host-charset",value="auto"},option={name="inferior-tty"},option={name="input-radix",value="10"},option={name="interactive-mode",value="auto"},option={name="language",value="auto"},option={name="libthread-db-search-path",value="$sdir:$pdir"},option={name="listsize",value="10"},optionlist={prefix="logging ",showlist={option={name="file",value="gdb.txt"},option={name="overwrite",value="off"},option={name="redirect",value="off"}}},option={name="max-completions",value="200"},option={name="max-user-call-depth",value="1024"},option={name="may-insert-breakpoints",value="on"},option={name="may-insert-fast-tracepoints",value="on"},option={name="may-insert-tracepoints",value="on"},option={name="may-interrupt",value="on"},option={name="may-write-memory",value="on"},option={name="may-write-registers",value="on"},optionlist={prefix="mem  ",showlist={option={name="inaccessible-by-default",value="on"}}},option={name="mi-async",value="off"},optionlist={prefix="mpx ",showlist={option={name="bound"}}},option={name="multiple-symbols",value="all"},option={name="non-stop",value="off"},option={name="observer",value="off"},option={name="opaque-type-resolution",value="on"},option={name="osabi"},option={name="output-radix",value="10"},option={name="overload-resolution",value="on"},option={name="p",showlist={option={name="address",value="on"},option={name="array",value="off"},option={name="array-indexes",value="off"},option={name="asm-demangle",value="off"},option={name="demangle",value="on"},option={name="elements",value="200"},option={name="entry-values",value="default"},option={name="frame-arguments",value="scalars"},option={name="inferior-events",value="off"},option={name="max-symbolic-offset",value="unlimited"},option={name="null-stop",value="off"},option={name="object",value="off"},option={name="pascal_static-members",value="on"},option={name="pretty",value="off"},optionlist={prefix="print raw ",showlist={option={name="frame-arguments",value="off"}}},option={name="repeats",value="10"},option={name="sevenbit-strings",value="on"},option={name="static-members",value="on"},option={name="symbol",value="on"},option={name="symbol-filename",value="off"},option={name="symbol-loading",value="full"},option={name="thread-events",value="on"},optionlist={prefix="print type ",showlist={option={name="methods",value="on"},option={name="typedefs",value="on"}}},option={name="union",value="on"},option={name="vtbl",value="off"}}},option={name="pagination",value="on"},option={name="paths"},option={name="pr",showlist={option={name="address",value="on"},option={name="array",value="off"},option={name="array-indexes",value="off"},option={name="asm-demangle",value="off"},option={name="demangle",value="on"},option={name="elements",value="200"},option={name="entry-values",value="default"},option={name="frame-arguments",value="scalars"},option={name="inferior-events",value="off"},option={name="max-symbolic-offset",value="unlimited"},option={name="null-stop",value="off"},option={name="object",value="off"},option={name="pascal_static-members",value="on"},option={name="pretty",value="off"},optionlist={prefix="print raw ",showlist={option={name="frame-arguments",value="off"}}},option={name="repeats",value="10"},option={name="sevenbit-strings",value="on"},option={name="static-members",value="on"},option={name="symbol",value="on"},option={name="symbol-filename",value="off"},option={name="symbol-loading",value="full"},option={name="thread-events",value="on"},optionlist={prefix="print type ",showlist={option={name="methods",value="on"},option={name="typedefs",value="on"}}},option={name="union",value="on"},option={name="vtbl",value="off"}}},optionlist={prefix="print ",showlist={option={name="address",value="on"},option={name="array",value="off"},option={name="array-indexes",value="off"},option={name="asm-demangle",value="off"},option={name="demangle",value="on"},option={name="elements",value="200"},option={name="entry-values",value="default"},option={name="frame-arguments",value="scalars"},option={name="inferior-events",value="off"},option={name="max-symbolic-offset",value="unlimited"},option={name="null-stop",value="off"},option={name="object",value="off"},option={name="pascal_static-members",value="on"},option={name="pretty",value="off"},optionlist={prefix="print raw ",showlist={option={name="frame-arguments",value="off"}}},option={name="repeats",value="10"},option={name="sevenbit-strings",value="on"},option={name="static-members",value="on"},option={name="symbol",value="on"},option={name="symbol-filename",value="off"},option={name="symbol-loading",value="full"},option={name="thread-events",value="on"},optionlist={prefix="print type ",showlist={option={name="methods",value="on"},option={name="typedefs",value="on"}}},option={name="union",value="on"},option={name="vtbl",value="off"}}},option={name="prompt",value="(gdb) "},optionlist={prefix="python ",showlist={option={name="print-stack",value="message"}}},option={name="radix"},option={name="range-stepping",value="on"},option={name="rec",showlist={optionlist={prefix="record btrace ",showlist={optionlist={prefix="record btrace bts ",showlist={option={name="buffer-size",value="65536"}}},optionlist={prefix="record btrace pt ",showlist={option={name="buffer-size",value="16384"}}},option={name="replay-memory-access",value="read-only"}}},optionlist={prefix="record full ",showlist={option={name="insn-number-max",value="200000"},option={name="memory-query",value="off"},option={name="stop-at-limit",value="on"}}},option={name="function-call-history-size",value="10"},option={name="insn-number-max"},option={name="instruction-history-size",value="10"},option={name="memory-query"},option={name="stop-at-limit"}}},optionlist={prefix="record ",showlist={optionlist={prefix="record btrace ",showlist={optionlist={prefix="record btrace bts ",showlist={option={name="buffer-size",value="65536"}}},optionlist={prefix="record btrace pt ",showlist={option={name="buffer-size",value="16384"}}},option={name="replay-memory-access",value="read-only"}}},optionlist={prefix="record full ",showlist={option={name="insn-number-max",value="200000"},option={name="memory-query",value="off"},option={name="stop-at-limit",value="on"}}},option={name="function-call-history-size",value="10"},option={name="insn-number-max"},option={name="instruction-history-size",value="10"},option={name="memory-query"},option={name="stop-at-limit"}}},optionlist={prefix="remote ",showlist={option={name="P-packet"},option={name="TracepointSource-packet",value="auto"},option={name="X-packet"},option={name="Z-packet",value="on"},option={name="access-watchpoint-packet",value="auto"},option={name="agent-packet",value="auto"},option={name="allow-packet",value="auto"},option={name="attach-packet",value="auto"},option={name="binary-download-packet",value="auto"},option={name="breakpoint-commands-packet",value="auto"},option={name="btrace-conf-bts-size-packet",value="auto"},option={name="btrace-conf-pt-size-packet",value="auto"},option={name="conditional-breakpoints-packet",value="auto"},option={name="conditional-tracepoints-packet",value="auto"},option={name="disable-btrace-packet",value="auto"},option={name="disable-randomization-packet",value="auto"},option={name="enable-btrace-bts-packet",value="auto"},option={name="enable-btrace-pt-packet",value="auto"},option={name="exec-file"},option={name="fast-tracepoints-packet",value="auto"},option={name="fetch-register-packet",value="auto"},option={name="fork-event-feature-packet",value="auto"},option={name="get-thread-information-block-address-packet",value="auto"},option={name="get-thread-local-storage-address-packet",value="auto"},option={name="hardware-breakpoint-limit",value="-1"},option={name="hardware-breakpoint-packet",value="auto"},option={name="hardware-watchpoint-length-limit",value="-1"},option={name="hardware-watchpoint-limit",value="-1"},option={name="hostio-close-packet",value="auto"},option={name="hostio-fstat-packet",value="auto"},option={name="hostio-open-packet",value="auto"},option={name="hostio-pread-packet",value="auto"},option={name="hostio-pwrite-packet",value="auto"},option={name="hostio-readlink-packet",value="auto"},option={name="hostio-setfs-packet",value="auto"},option={name="hostio-unlink-packet",value="auto"},option={name="hwbreak-feature-packet",value="auto"},option={name="install-in-trace-packet",value="auto"},option={name="interrupt-on-connect",value="off"},option={name="interrupt-sequence",value="Ctrl-C"},option={name="kill-packet",value="auto"},option={name="library-info-packet",value="auto"},option={name="library-info-svr4-packet",value="auto"},option={name="memory-map-packet",value="auto"},option={name="memory-read-packet-size"},option={name="memory-write-packet-size"},option={name="noack-packet",value="auto"},option={name="osdata-packet",value="auto"},option={name="p-packet"},option={name="pass-signals-packet",value="auto"},option={name="pid-to-exec-file-packet",value="auto"},option={name="program-signals-packet",value="auto"},option={name="query-attached-packet",value="auto"},option={name="read-aux-vector-packet",value="auto"},option={name="read-btrace-conf-packet",value="auto"},option={name="read-btrace-packet",value="auto"},option={name="read-fdpic-loadmap-packet",value="auto"},option={name="read-sdata-object-packet",value="auto"},option={name="read-siginfo-object-packet",value="auto"},option={name="read-spu-object-packet",value="auto"},option={name="read-watchpoint-packet",value="auto"},option={name="reverse-continue-packet",value="auto"},option={name="reverse-step-packet",value="auto"},option={name="run-packet",value="auto"},option={name="search-memory-packet",value="auto"},option={name="set-register-packet",value="auto"},option={name="software-breakpoint-packet",value="auto"},option={name="static-tracepoints-packet",value="auto"},option={name="supported-packets-packet",value="auto"},option={name="swbreak-feature-packet",value="auto"},option={name="symbol-lookup-packet",value="auto"},option={name="system-call-allowed"},option={name="target-features-packet",value="auto"},option={name="threads-packet",value="auto"},option={name="trace-buffer-size-packet",value="auto"},option={name="trace-status-packet",value="auto"},option={name="traceframe-info-packet",value="auto"},option={name="unwind-info-block-packet",value="auto"},option={name="verbose-resume-packet",value="auto"},option={name="vfork-event-feature-packet",value="auto"},option={name="write-siginfo-object-packet",value="auto"},option={name="write-spu-object-packet",value="auto"},option={name="write-watchpoint-packet",value="auto"}}},option={name="remoteaddresssize",value="0"},option={name="remotebreak",value="off"},option={name="remotecache",value="off"},option={name="remoteflow",value="off"},option={name="remotelogbase",value="ascii"},option={name="remotelogfile"},option={name="remotetimeout",value="2"},option={name="remotewritesize"},option={name="schedule-multiple",value="off"},option={name="scheduler-locking",value="off"},option={name="script-extension",value="soft"},optionlist={prefix="serial ",showlist={option={name="baud",value="-1"},option={name="parity",value="none"}}},option={name="solib-absolute-prefix"},option={name="solib-search-path"},option={name="stack-cache",value="on"},option={name="startup-with-shell",value="on"},option={name="step-mode",value="off"},option={name="stop-on-solib-events",value="0"},option={name="struct-convention",value="default"},option={name="substitute-path"},option={name="sysroot",value="target:"},option={name="target-async"},option={name="target-charset",value="auto"},option={name="target-file-system-kind",value="auto"},option={name="target-wide-charset",value="auto"},optionlist={prefix="tcp ",showlist={option={name="auto-retry",value="on"},option={name="connect-timeout",value="15"}}},optionlist={prefix="tdesc ",showlist={option={name="filename"}}},option={name="trace-buffer-size",value="unlimited"},option={name="trace-commands",value="off"},option={name="trace-notes"},option={name="trace-stop-notes"},option={name="trace-user"},option={name="trust-readonly-sections",value="off"},optionlist={prefix="tui ",showlist={option={name="active-border-mode",value="bold-standout"},option={name="border-kind",value="acs"},option={name="border-mode",value="normal"}}},option={name="unwind-on-terminating-exception",value="on"},option={name="unwindonsignal",value="off"},option={name="use-coredump-filter",value="on"},option={name="use-deprecated-index-sections",value="off"},option={name="user"},option={name="verbose",value="off"},option={name="watchdog",value="0"},option={name="width",value="133"},option={name="write",value="off"}}
"""
},
{
"10-data-list-register-names",
r"""
10^done,register-names=["eax","ecx","edx","ebx","esp","ebp","esi","edi","eip","eflags","cs","ss","ds","es","fs","gs","st0","st1","st2","st3","st4","st5","st6","st7","fctrl","fstat","ftag","fiseg","fioff","foseg","fooff","fop","xmm0","xmm1","xmm2","xmm3","xmm4","xmm5","xmm6","xmm7","mxcsr","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","orig_eax","al","cl","dl","bl","ah","ch","dh","bh","ax","cx","dx","bx","","bp","si","di","mm0","mm1","mm2","mm3","mm4","mm5","mm6","mm7"]
"""
}
}



#show threads or something
#sample2 = r"""
#10^done,features=["frozen-varobjs","pending-breakpoints","thread-info","data-read-memory-bytes","breakpoint-notifications","ada-task-info","language-option","info-gdb-mi-command","undefined-command-error-code","exec-run-start-option","python"]
#"""


mi2_commands = {
"-break-after",
"-break-condition",
"-break-delete",
"-break-disable",
"-break-enable",
"-break-info",
"-break-insert",
"-break-list",
"-break-watch",
"-data-disassemble",
"-data-evaluate-expression",
"-data-list-changed-registers",
"-data-list-register-names",
"-data-list-register-values",
"-data-read-memory",
"-display-delete",
"-display-disable",
"-display-enable",
"-display-insert",
"-display-list",
"-environment-cd",
"-environment-directory",
"-environment-path",
"-environment-pwd",
"-exec-abort",
"-exec-arguments",
"-exec-continue",
"-exec-finish",
"-exec-interrupt",
"-exec-next",
"-exec-next-instruction",
"-exec-return",
"-exec-run",
"-exec-show-arguments",
"-exec-step",
"-exec-step-instruction",
"-exec-until",
"-file-exec-and-symbols",
"-file-exec-file",
"-file-list-exec-sections",
"-file-list-exec-source-files",
"-file-list-shared-libraries",
"-file-list-symbol-files",
"-file-symbol-file",
"-gdb-exit",
"-gdb-set",
"-gdb-show",
"-gdb-version",
"-stack-info-frame",
"-stack-info-depth",
"-stack-list-arguments",
"-stack-list-frames",
"-stack-list-locals",
"-stack-select-frame",
"-symbol-info-address",
"-symbol-info-file",
"-symbol-info-function",
"-symbol-info-line",
"-symbol-info-symbol",
"-symbol-list-functions",
"-symbol-list-types",
"-symbol-list-variables",
"-symbol-locate",
"-symbol-type",
"-target-attach",
"-target-compare-sections",
"-target-detach",
"-target-download",
"-target-exec-status",
"-target-list-available-targets",
"-target-list-current-targets",
"-target-list-parameters",
"-target-select",
"-thread-info",
"-thread-list-all-threads",
"-thread-list-ids",
"-thread-select",
"-var-create",
"-var-delete",
"-var-set-format",
"-var-show-format",
"-var-info-num-children",
"-var-list-children",
"-var-info-type",
"-var-info-expression",
"-var-show-attributes",
"-var-evaluate-expression",
"-var-assign",
"-var-update",
}

if __name__ == "__main__":
    for test in tests:
        print("1")
        parsed_output = mi2_parser.parse_mi2(test[2])
        print(parsed_output)



