
This is meant to be a graphical front end for gdb. I intend for it to have the
same sort of capabilities as ddd but hopefully look better and be easier to use.

It will also be a graphical front end for valgrind so that it is possible to see
things such as the amount of memory that is currently being used.
