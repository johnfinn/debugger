#!/bin/python2
from pyparsing import *



# TODO Parse the mi2 format into something python can understand



def parse_failed(s, loc, expr, err):
    print("Parse Failure".center(50, "="))
    print("String:", s)
    print("Location:", loc)
    print("Expression:", expr)
    print("Error:", err)
    print("=" * 50)

# Defining the grammar
LBRACE=Suppress("{")
RBRACE=Suppress("}")
LBRACKET=Suppress("[")
RBRACKET=Suppress("]")
COMMA=Suppress(",")

nl     = Literal(r"\r") | Literal(r"\r\n")
token  = Word(nums)
c_string = QuotedString("\"", escChar='\\', multiline=True)
c_string.setFailAction(parse_failed)

done        = Keyword("done")
running     = Keyword("running")
connected   = Keyword("connected")
error       = Keyword("error")
exit        = Keyword("exit")
stopped     = Keyword("stopped")

console_stream_output = \
    Literal("~") + c_string# + nl
target_stream_output = \
    Literal("@") + c_string# + nl
log_stream_output = \
    Literal("&") + c_string# + nl
stream_record = \
    (console_stream_output | target_stream_output | log_stream_output)

result_class = (done | running | connected | error | exit)
async_class  = stopped
const  = QuotedString("\"")
value  = Forward()
result = Forward()
_tuple = \
Group(LBRACE+RBRACE | LBRACE + result + Optional(OneOrMore( COMMA + result )) + RBRACE)
_list  = \
Group(LBRACKET+RBRACKET | LBRACKET + value  + Optional(OneOrMore( COMMA + value  )) + RBRACKET | \
LBRACKET + result + Optional(OneOrMore( COMMA + result )) + RBRACKET)

variable = Word(alphanums)
value << (const | _tuple | _list)
result << Group(variable + Suppress("=") + value)

async_output = \
    async_class + Optional(OneOrMore( "," + result ))
notify_async_output = \
    Optional( token ) + "=" + async_output + nl
status_async_output = \
    Optional( token ) + "+" + async_output + nl
exec_async_output = \
    Optional( token ) + "*" + async_output + nl
async_record = \
    (exec_async_output | status_async_output | notify_async_output)
out_of_band_record = \
    (async_record | stream_record)
result_record = \
    Optional( token ) + "^" + result_class + Optional(OneOrMore( Suppress(",") + result ))# + nl
prompt = \
    Optional("(gdb)")# + nl
output = \
    Optional(OneOrMore(Group(out_of_band_record))) + Optional( result_record ) + prompt

# set the names of the results

token.setResultsName("token")
c_string.setName("string")

# set the names of the parsers for debugging purposes

token.setName("token")
nl.setName("newline")
c_string.setName("c_string")
done.setName("done")
running.setName("running")
connected.setName("connected")
error.setName("error")
exit.setName("exit")
stopped.setName("stopped")
console_stream_output.setName("console_stream_output")
target_stream_output.setName("target_stream_output")
log_stream_output.setName("log_stream_output")
stream_record.setName("stream_record")
result_class.setName("result_class")
variable.setName("variable")
value.setName("value")
result.setName("result")
async_output.setName("async_output")
notify_async_output.setName("notify_async_output")
status_async_output.setName("status_async_output")
exec_async_output.setName("exec_async_output")
async_record.setName("async_record")
out_of_band_record.setName("out_of_band_record")
result_record.setName("result_record")

#mi2_grammar = Dict(Group("(gdb)\r"))
#mi2_grammar = Dict(Group(line))
#mi2_grammar = Dict(OneOrMore(Group(output)))

#result = mi2_grammar.parseString("(gdb)")
#print result


mi2_grammar = output
mi2_grammar.setFailAction(parse_failed)

def parse_mi2(text):
    try:
        return mi2_grammar.parseString(text, parseAll=True)
    except ParseException:
        return ""

def debug_on():
    mi2_grammar.setDebug(True)
    output.setDebug(True)
    out_of_band_record.setDebug(True)
    result_record.setDebug(True)
    result.setDebug(True)
    prompt.setDebug(True)


#debug_on()


#mi2_grammar_bnf = """
#output:
#    ( out-of-band-record )* [ result-record ] "(gdb)" nl
#result-record:
#    [ token ] "^" result-class ( "," result )* nl
#out-of-band-record:
#    async-record | stream-record
#async-record:
#    exec-async-output | status-async-output | notify-async-output
#exec-async-output:
#    [ token ] "*" async-output nl
#status-async-output:
#    [ token ] "+" async-output nl
#notify-async-output:
#    [ token ] "=" async-output nl
#async-output:
#    async-class ( "," result )*
#
#done:       Word("done")
#running:    Word("running")
#connected:  Word("connected")
#error:      Word("error")
#exit:       Word("exit")
#stopped:    Word("stopped")
#
#result-class:
#    done | running | connected | error | exit
#async-class:
#    stopped
#result:
#    variable "=" value
#variable:
#    Word(alphanums)
#value:
#    const | tuple | list
#const:
#    Word(alphanums)
#tuple:
#    "{}" | "{" result ( "," result )* "}"
#list:
#    "[]" | "[" value ( "," value )* "]" | "[" result ( "," result )* "]"
#stream-record:
#    console-stream-output | target-stream-output | log-stream-output
#console-stream-output:
#    "~" + c-string + nl
#target-stream-output:
#    "@" + c-string + nl
#log-stream-output:
#    "&" + c-string + nl
#nl:
#    "\r" | "\r\n"
#token:
#    Word( digits )
#digits: "0123456789"
#"""
#
#
#valgrind_grammar_bnf = """
#pid: "==" + digits + "== "
#output: pid + message
#"""
#
#valgrind_grammar = Dict(OneOrMore(Group(Suppress("==") + Word( digits ) + Suppress("== ") + restOfLine)))
#
#valgrind_output = """
#==9885== Memcheck, a memory error detector
#==9885== Copyright (C) 2002-2013, and GNU GPL'd, by Julian Seward et al.
#==9885== Using Valgrind-3.10.1 and LibVEX; rerun with -h for copyright info
#==9885== Command: ./a.out
#==9885== 
#This is a very simple program
#i == 0
#i == 1
#i == 2
#i == 3
#i == 4
#i == 5
#i == 6
#i == 7
#i == 8
#i == 9
#Exiting
#==9885== 
#==9885== HEAP SUMMARY:
#==9885==     in use at exit: 0 bytes in 0 blocks
#==9885==   total heap usage: 0 allocs, 0 frees, 0 bytes allocated
#==9885== 
#==9885== All heap blocks were freed -- no leaks are possible
#==9885== 
#==9885== For counts of detected and suppressed errors, rerun with: -v
#==9885== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
#"""
#
#result = valgrind_grammar.parseString(valgrind_output)
#print result

#class Mi2:
#    """ Parses the mi2 format so that it can be output in a more readable form """
#
#    def __init__(self, mi2_string):
#        self.mi2_string = mi2_string
#
#    def to_json(self):
#        """ Return the data received in mi2 format in json format """
#
#    def to_xml(self):
#        """ Return the data received in mi2 format in xml format """


#
#"==20836== Memcheck, a memory error detector"
#
# Becomes
#
#"""
#"valgrind": {
#    "pid": 20836,
#    "message": "Memcheck, a memory error detector"
#}
#"""
