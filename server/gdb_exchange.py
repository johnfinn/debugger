
# client side

# to communicate with gdb you create an "exchange"
# The exchange knows what to do when the response to a message is received

class GDB_command:
    event_id = None
    status = None

    def __init__(self, handler):
        self.handler = handler

    def request(self, command):
        self.event_id = self.handler.register(filter={"CONTROL":"RESPONSE"}, self.response)

        message = {
                  "CONTROL" : "REQUEST", 
                  "COMMAND" : "interpreter-exec mi2 \"%s\"" % command
                  }
        self.send(message)

    def response(self, stream, message):
        print(message["TEXT"])


# server side
self.handler.register("REQUEST", self.gdb_request)

def gdb_request(self, stream, message):

    command = message["COMMAND"]
    response["CONTROL"] = "RESPONSE"
    response["TEXT"] = gdb.execute(command, from_tty=False, to_string=True)
    self.socket.send_json(response)
