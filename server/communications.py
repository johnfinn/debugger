#!/bin/python2
import zmq
import time
import sys
import json
import logging
from collections import deque
import re
import threading
import multiprocessing

log = logging.getLogger("logger")
log.setLevel(logging.INFO)
log.propagate = False
handler   = logging.StreamHandler()
#formatter = logging.Formatter("%(name)s %(thread)d %(levelname)s %(asctime)s %(funcName)s   | %(message)s")
#handler.setFormatter(formatter)
log.addHandler(handler)

from zmq.eventloop.zmqstream import ZMQStream

from zmq.eventloop import ioloop
ioloop.install()

context  = zmq.Context()

#def create_pipe():
#    a_socket = context.socket(zmq.PAIR)
#    b_socket = context.socket(zmq.PAIR)
#    a_socket.bind("inproc://pair_socket")
#    b_socket.connect("inproc://pair_socket")
#    a_stream = ZMQStream(a_socket)
#    b_stream = ZMQStream(b_socket)
#
#    return a_stream, b_stream

#class Agent:
#
#
#    def add_pipe(self, name, stream):
#        pipes[name] = stream


    #def alive(self):
    #    """ Returns true if still connected to a server """

class Exchange:
    expired = False
    status  = None
    handler = None

    def __init__(self, handler):
        self.handler = handler
        self.timeout = 1

    def send(self, message):
        print("Sending")

    def change_status(self, status):
        self.status = status
        # TODO iterate through the callbacks waiting for this status and notify them

    def get_expired(self):
        if self.expired == False:
            if time.time() > self.expired_time:
                self.expired = True
        return self.expired

    def get_status(self):
        if self.status == "WAITING":
            if self.get_expired():
                self.status == "FAILED"
        return self.status

class Deregistration:

    def __init__(self, handler):
        self.handler = handler
        
        self.request()

    def request(self):
        #message = { "CONTROL" : "DEREGISTER", "NAME" : "TESTING" }
        message = ["REGISTER", "client name"]
        self.handler.send(message)


class Registration:
    event_id = None
    status = None

    def __init__(self, handler):
        #Exchange.__init__(self, handler)
        self.handler = handler
        
        self.request()

    def request(self):
        self.event_id = self.handler.register("REGISTER", self.response)

        #message = { "CONTROL" : "REGISTER", "NAME" : "TESTING" }
        message = ["REGISTER", "client name"]
        self.handler.send(message)

    def response(self, message):
        try:
            if message[2] == "ACCEPT":
                self.accept(message)
            elif message[2] == "DENY":
                self.deny(message)
            else:
                raise

            self.handler.deregister(self.event_id)

        except Exception as e:
            log.error("%r" % e)

    def accept(self, message):
        print("registration accepted")

    def deny(self, message):
        print("registration denied")

class Heartbeat:
    event_id = None
    expired = False
    status = None

    def __init__(self, handler):
        self.handler = handler
        self.timeout = 1

        self.request()

    def request(self):
        self.event_id = self.handler.register("PONG", self.response)

        #message = {"CONTROL":"PING"]
        message = ["PING"]
        self.expired_time = time.time() + self.timeout
        self.status = "WAITING"
        self.handler.send(message)

    def response(self, message):
        self.handler.deregister(self.event_id)
        if self.get_expired():
            self.status = "FAILED"
        else:
            self.status = "SUCCESS"

    def get_expired(self):
        if self.expired == False:
            if time.time() > self.expired_time:
                self.expired = True
        return self.expired

    def get_status(self):
        if self.status == "WAITING":
            if self.get_expired():
                self.status == "FAILED"
        return self.status

class ZMQAgent:
    instance     = None
    agent_thread = None

    def __init__(self):
        self.instance = ioloop.IOLoop.instance()
        self.agent_thread = threading.Thread(target=self.instance.start)

    def start(self):
        log.info("Starting IOLoop")
        self.agent_thread.start()

    def stop(self):
        log.info("Stopping IOLoop")
        self.instance.stop()
        self.wait()

    def wait(self):
        self.agent_thread.join()

class Client(ZMQAgent):
    socket   = None
    endpoint = "commands.ipc"

    def __init__(self):
        ZMQAgent.__init__(self)
        self.socket = context.socket(zmq.DEALER)
        self.socket.connect("ipc:///tmp/%s" % self.endpoint)

        self.handler = message_handler(self.socket, self.instance)

    def register(self):
        return Registration(self.handler)

    def deregister(self):
        message = ["DEREGISTER"]
        return Deregistration(self.handler)

    def heartbeat(self):
        return Heartbeat(self.handler)

class Peer:
    joined = None
    last_heartbeat = None
    status = None

    def __init__(self):
        self.joined = time.time()

class Server(ZMQAgent):
    socket   = None
    endpoint = "commands.ipc"
    sleep_time = 0

    peers = None

    def __init__(self):
        ZMQAgent.__init__(self)
        self.socket = context.socket(zmq.ROUTER)
        self.socket.bind("ipc:///tmp/%s" % self.endpoint)

        self.handler = message_handler(self.socket, self.instance)
        self.handler.register("REGISTER", self.register_request)
        self.handler.register("DEREGISTER", self.deregister_request)
        self.handler.register("PING", self.heartbeat)

        self.peers = {}

    def register_request(self, message):
        log.info("Register requested %r" % message)
        self.peers[message[0]] = Peer()
        response = [message[0], "", "REGISTER", "ACCEPT"]
        log.info("Response %r" % response)
        self.handler.send(response)

    def deregister_request(self, message):
        log.info("Deregister requested %r" % message)
        del self.peers[message[0]]
        response = [message[0], "", "DEREGISTER", "ACCEPT"]
        log.info("Response %r" % response)
        self.handler.send(response)

    def heartbeat(self, message):
        log.info("Heartbeat received %r" % message)
        self.sleep_time += 1
        if self.sleep_time % 2 == 0:
            time.sleep(self.sleep_time)

        self.handler.send([message[0], "PONG"])

class message_handler:
    handlers = None

    socket = None
    stream = None

    def __init__(self, socket, instance):
        self.socket = socket
        self.stream = ZMQStream(socket, instance)
        self.stream.on_recv_stream(self.callback)

        self.handlers = { "A": "B"}

    def callback(self, stream, message):
        log.debug("message %r" % message)

        if len(message) == 1:
            header = message[0]
        else:
            header = message[1]

        match = False
        try: 
            for event, event_callback in self.handlers.iteritems():
                if event == header:
                    match = True
                    break
            if match:
                event_callback(message)
        except TypeError as e:
            log.error(e)

        if match == False:
            self.no_match(stream, message)

    def no_match(self, address):
        """ This is called when no handler matches the message received """
        print("no match")
        response = []
        #response.append(address)
        #response.append("")
        response.append("no match")

    # TODO return an event id so the event can later be removed
    def register(self, event, callback, priority=0):
        """ Register a new callback for when a message matches an event """
        self.handlers[event] = callback
        log.debug("New handler registered (%s : %s)" % (event, callback) )
        return event

    def deregister(self, event):
        log.debug("Removing event handler")
        #del self.handlers[event] 

    def send(self, message):
        self.socket.send_multipart(message)
        

#class comms:
#    pipe  = None
#    agent = None
#
#    def __init__(self, agent):
#        self.agent = agent
#        self.pipe, agent_pipe = create_pipe()
#        self.agent.add_pipe(out_pipe)
#
#    def send(self, message):
#        """ send a message on the pipe """
#        self.pipe.send_multipart(message)
#
#    def recv(self):
#        """ receive a message from the pipe """
#        return self.pipe.recv_multipart()
#
#
#
#def client_loop():
#    log.info("Client loop")
#    request = ""
#    while True:
#        request = ["REQUEST"]
#        request.append(raw_input("IN: "))
#        log.info("%r" % request)
#        if request != None:
#            com.send(request)
#            message = com.recv()
#            log.info("%r" % message)
#        else:
#            print "Nothing to send"

if __name__ == "__main__":
    import sys
    try:
        if sys.argv[1] == "Client":
            agent = Client()
            agent.start()
            agent.register()
            while True:
                heartbeat = agent.heartbeat()
                while heartbeat.get_status() == "WAITING":
                    time.sleep(1)
                status = heartbeat.get_status()
                print("heartbeat status %s" % status)
            ##agent.wait()
            #time.sleep(100)
        elif sys.argv[1] == "Server":
            agent = Server()
            agent.start()
            #agent.wait() # wait for the server thread to be done
            time.sleep(100)
        else:
            log.error("Unrecognized role: %s" % sys.argv[1])
            
    except KeyboardInterrupt as e:
        print("Exiting")
    #except Exception as e:
    #    log.error(e)
    finally:
        try:
            agent.stop()
        except NameError as e:
            log.error("No agent to stop")





#["REQUEST",
#
#message = ["REGISTER", "client name", "client id", "expect heartbeat", "Version", "Requested version"]
#["REGISTER", version ]
#["REGISTER", "ACCEPT", reason ]
#["REGISTER", "DENY", reason ]
#
#["DEREGISTER"]
#
#["TERMINATING"]                    # When a server instance is going down
#["STARTING"]                       # when a server instance is starting possibly
                                    # replacing an old instance
#
#["PING", unique_id]
#["PONG", unique_id]



