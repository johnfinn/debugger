#!/bin/python2
import subprocess
import json
import zmq
import mi2_parser
import logging
import time
from threading import Thread

"""
This should receive messages that are to be run by gdb then forward them to the 
python script that gdb is running over zeromq

The python script should only communicate with this script and all other scripts
must go through it.
"""


log = logging.getLogger("log")
log.setLevel(logging.WARNING)
handler = logging.StreamHandler()
formatter = logging.Formatter("%(levelname)s %(message)s")
handler.setFormatter(formatter)
log.addHandler(handler)

class communications:
    def __init__(self):
        self.context  = zmq.Context()
        self.event_sub = self.context.socket(zmq.SUB)
        self.event_pub = self.context.socket(zmq.PUB)
        self.event_sub.bind("ipc:///tmp/events_sink")
        self.event_sub # subscribe to all events
        self.event_pub.bind("ipc:///tmp/events")

    def close(self):
        self.event_pub.close()
        self.event_sub.close()

com = communications()

# this should be in a thread
#proxy(event_sub, event_pub)

def parse_arguments():
    """Parse the program arguments"""
    from argparse import ArgumentParser, FileType

    parser = ArgumentParser()
    parser.add_argument("--version", help="Show version and exit")
    parser.add_argument("-v", "--verbosity", action="count",
                      help="increase verbosity")
    parser.add_argument("-d", "--debug", action="store_true",
                      help="activate_debug_mode")
    parser.add_argument("-g", "--gdb", type=FileType("r"),
                      help="Invoke GDB as debugger")
    parser.add_argument("--log", type=int)
    args = parser.parse_args()

    # TODO figure out how to do defaults with the arguments
    args.gdb=True

    if args.verbosity == 1:
        log.setLevel(logging.INFO)
    if args.verbosity >= 2 or args.debug:
        log.setLevel(logging.DEBUG)

parse_arguments()

class gdb:

    arguments = None
    status    = None
    process   = None
    stdout_thread = None
    stderr_thread = None

    def __init__(self):
        self.arguments = ["gdb", 
                         "--statistics", 
                         "--silent",            # don't print version info
                         "--interpreter=mi2",   # use the mi2 interpreter
                         "--annotate=3",
                         "--batch",             # close after the script exits
                         "--command=server.py"  # run our python script on start
                        ]
        self.status = "uninitialized"

    def start(self):

        self.change_status("starting")
        try:
            self.process = subprocess.Popen(self.arguments, 
                                        stdin=subprocess.PIPE,
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE,
                                        )
            self.change_status("started")
            # TODO deal with the python script running inside gdb possibly breaking

            self.stdout_thread = Thread(target=self.parse, args=(self.process.stdout,))
            self.stderr_thread = Thread(target=self.log,   args=(self.process.stderr,))
            self.stdout_thread.start()
            self.stderr_thread.start()

            return_code = self.process.wait()
            self.stdout_thread.join()
            self.stderr_thread.join()


            if return_code == 0:
                log.info("gdb exited")
                self.change_status("exited")
            else:
                log.error("gdb error %d", return_code)
                self.change_status("failed")

            #stdout, stderr = self.process.communicate() 
            #log.error("stderr: %s", stderr)

            #parsed_output = mi2_parser.parse_mi2(stdout)
            #log.debug("stdout: %s", stdout)
            #log.debug("parsed: %s", parsed_output)

        except OSError as e:
            self.change_status("failed")
            log.critical("gdb: %s", e)

    def parse(self, pipe):
        while self.process.poll() is None:
            stdout = pipe.readline()
            if stdout != "":
                parsed_output = mi2_parser.parse_mi2(stdout)
                log.debug("output: %s", stdout)
                log.debug("parsed: %s", parsed_output)

    def log(self, pipe):
        while self.process.poll() is None:
            stderr = pipe.readline()
            if stderr != "":
                log.error("stderr: %s", stderr)


    def change_status(self, status):
        old_status = self.status
        self.status = status
        log.info("gdb: %s -> %s", old_status, self.status)

    def stop(self):
        """ Attempts to stop gdb if its unsuccessful gdb is killed """
        self.change_status("stopping")
        return_code = None
        for i in range(0,3):
            return_code = self.process.poll()
            if return_code == None:
                self.process.terminate()
                time.sleep(1)
            else:
                break

        if return_code == None:
            self.kill()
        else:
            self.change_status("stopped")

    def kill(self):
        self.change_status("killing")
        self.process.kill()
        return_code = self.process.wait()
        if return_code < 0:
            self.change_status("killed")
        else:
            log.critical("gdb could not be stopped")
        

def start_valgrind():
    print "starting valgrind is not allowed yet"
    #valgrind_arguments = ["valgrind", 
    #                      "--vgdb=yes", 
    #                      "--vgdb-error=0", 
    #                      "--xml-fd=10", 
    #                      program]

try:
    global gdb_instance 
    gdb_instance = gdb()
    gdb_instance.start()
except KeyboardInterrupt as e:
    print "Stopping"
    log.debug("KeyboardInterrupt Exiting")
finally:
    gdb_instance.stop()
    com.close()


