#!/bin/python2
import zmq
import time
import sys
import json
import logging


log = logging.getLogger("log")
log.setLevel(logging.WARNING)
handler = logging.StreamHandler()
formatter = logging.Formatter("%(levelname)s %(asctime)s %(funcName)s %(message)s")
handler.setFormatter(formatter)
log.addHandler(handler)

from zmq.eventloop.zmqstream import ZMQStream

from zmq.eventloop import ioloop
ioloop.install()

class communications:
    stream = None

    def __init__(self):
        self.context  = zmq.Context()
        #self.commands = self.context.socket(zmq.REP)
        #self.events   = self.context.socket(zmq.PUB)
        #self.commands.bind("ipc:///tmp/commands")
        #self.events.bind("ipc:///tmp/events_sink")

        
        socket = self.context.socket(zmq.ROUTER)
        socket.bind("ipc:///tmp/commands")
        self.stream = ZMQStream(socket)

    def start(self):
        ioloop.IOLoop.instance().start()

com = communications()

#print "Architecture: ", gdb.Architecture.name()
# TODO handle errors that may come up
# gdb.error and gdb.MemoryError are good ones

# Figure out how to deal with logging

#def event_to_json(event):
#    """ Turns an event into json so it can be sent to other processes """
#    json_str = "'event': "
#    json_str += "{ 'type': ", type(event)
#    if type(event) == ContinueEvent:
#        json_str += "'inferior':", event.inferior_thread
#    elif event.type == exited:
#        json_str += "'exit_code':", event.exit_code
#        json_str += "'inferior':", event.inferior
#    elif event.type == stop:
#        json_str += "'stop_signal':", event.stop_signal
#        json_str += "'breakpoints':", event.breakpoints
#    elif event.type == new_objfile:
#        json_str += "'objfile':", event.new_objfile
#    elif event.type == clear_objfile:
#        json_str += "'progspace':", event.progspace
#    elif event.type == inferior_call_pre:
#        json_str += "'ptid':", event.ptid
#        json_str += "'address':", event.address
#    elif event.type == inferior_call_post:
#        json_str += "'ptid':", event.ptid
#        json_str += "'address':", event.address
#    elif event.type == memory_changed:
#        json_str += "'address':", event.address
#        json_str += "'length':", event.length
#    elif event.type == register_changed:
#        json_str += "'frame':", event.frame
#        json_str += "'regnum':", event.regnum
#    json_str += "}"
#    return json_str
#
#def event_handler(event):
#    """ For now just print the json instead of publishing it """
#    print("Event")
#    #print "Event: ", event_to_json(event)
#
#
#def register_event_handlers():
#    gdb.events.cont.connect( event_handler )
#    gdb.events.exited.connect( event_handler )
#    gdb.events.stop.connect( event_handler )
#    gdb.events.new_objfile.connect( event_handler )
#    gdb.events.clear_objfile.connect( event_handler )
#    gdb.events.inferior_call_pre.connect( event_handler )
#    gdb.events.inferior_call_post.connect( event_handler )
#    gdb.events.memory_changed.connect( event_handler )
#    gdb.events.register_changed.connect( event_handler )

def gdb_execute(stream, message):
    try:
        print("Request %r" % message)
        command = message[2].decode("ascii")
        response = [message[0]]
        response.append(gdb.execute(command, from_tty=False, to_string=True))
        response[1].encode("ascii")
        print("Response %r" % response)
        stream.socket.send_multipart(response)
    except gdb.error as e:
        log.exception("gdb exception %s", e)
        stream.socket.send_multipart("%r" % e)
    except Exception as e:
        print(e)

com.stream.on_recv_stream(gdb_execute)
com.start()

# add new gdb commands to control the server
#def server_loop():
#    while True:
#        # Wait for next request from client
#        try:
#            message = com.recv()
#            response = gdb.execute(message, from_tty=False, to_string=True)
#            com.resp("mi2_response: {%s}" % response)
#        except gdb.error as e:
#            log.exception("gdb exception %s", e)
#            com.resp("%r" % e)
#        except ZMQError as e:
#            log.exception("Zeromq exception %s", e)
#
#try:
#    com.event("gdb command server starting")
#    server_loop()
#    com.event("gdb command server stopping")
#finally:
#    com.close()

