/*******************************************************************************
 *       Filename:  hello_world.c
 *    Description:  Prints hello world
 *         Author:  John Finn
 ******************************************************************************/
#include <stdlib.h>
#include <stdio.h>

int main() {

    printf("hello world\n");

}
