/*******************************************************************************
 *       Filename:  loop.c
 *    Description:  Prints in a loop
 *         Author:  John Finn
 ******************************************************************************/
#include <stdlib.h>
#include <stdio.h>

void loop() {
    int i = 0;
    for( i = 0; i < 10; ++i ) {
        printf("i == %d\n", i);
    }
}

int main() {

    printf("This is a very simple program\n");

    loop();

    printf("Exiting\n");

}
