/*******************************************************************************
 *       Filename:  null_ptr_deref.c
 *    Description:  Causes a segmentation fault
 *         Author:  John Finn, 
 ******************************************************************************/
#include <stdlib.h>
#include <stdio.h>

int main() {
    int *seg_fault = NULL;
    (*seg_fault) = 1;
}

