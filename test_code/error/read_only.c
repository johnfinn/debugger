/*******************************************************************************
 *       Filename:  read_only.c
 *    Description:  Causes a segmentation fault
 *         Author:  John Finn, 
 ******************************************************************************/
#include <stdlib.h>
#include <stdio.h>

int main() {
    char *seg_fault = "seg_fault";
    seg_fault[0] = "A";
}

