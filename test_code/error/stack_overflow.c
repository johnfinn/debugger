/*******************************************************************************
 *       Filename:  seg_fault2.c
 *    Description:  Causes segmentation fault
 *         Author:  John Finn, 
 ******************************************************************************/
#include <stdlib.h>
#include <stdio.h>

int main() {
    main();
    return 0;
}

